from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views

# Create your tests here.

class TestUrls(TestCase):
    def test_index_url_resolved(self):
        url = reverse('lab_9:index')
        self.assertEquals(resolve(url).func, views.index)
    def test_login_url_resolved(self):
        url = reverse('lab_9:login')
        self.assertEquals(resolve(url).func, views.login)
    def test_register_url_resolved(self):
        url = reverse('lab_9:register')
        self.assertEquals(resolve(url).func, views.register)
    def test_logout_url_resolved(self):
        url = reverse('lab_9:logout')
        self.assertEquals(resolve(url).func, views.logout)

class TestViews(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'
        }
        User.objects.create_user(**self.credentials)
    def test_login_GET(self):
        client = Client()
        response = client.get(reverse('lab_9:login'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'lab_9/login.html')
    def test_register_GET(self):
        client = Client()
        response = client.get(reverse('lab_9:register'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'lab_9/register.html')
    def test_index_without_login_GET(self):
        response = self.client.get(reverse('lab_9:index'), follow=True)
        self.assertRedirects(response, reverse('lab_9:login'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
    def test_index_with_login_GET(self):
        response = self.client.post(reverse('lab_9:login'), self.credentials, follow=True)
        self.assertRedirects(response, reverse('lab_9:index'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
        self.assertTemplateUsed(response,  'lab_9/index.html')

class TestLogin(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'
        }
        self.falseCredentials = {
            'username': 'testuser2',
            'password': 'secret2'
        }
        User.objects.create_user(**self.credentials)
    def test_successful_login(self):
        response1 = self.client.post(reverse('lab_9:login'), self.credentials, follow=True)
        self.assertRedirects(response1, reverse('lab_9:index'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)

        response2 = self.client.get(reverse('lab_9:login'), follow=True)
        self.assertRedirects(response2, reverse('lab_9:index'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
    def test_unsuccessful_login(self):
        response = self.client.post(reverse('lab_9:login'), self.falseCredentials, follow=True)
        self.assertEquals(response.status_code, 401)
        self.assertTemplateUsed(response, 'lab_9/login.html')

class TestRegister(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password1': '1@2H3f4@5n',
            'password2': '1@2H3f4@5n'
        }
        self.falseCredentials = {
            'username': 'testuser2',
            'password1': '123456ok',
            'password2': '123456ok'
        }
    def test_successful_register(self):
        response1 = self.client.post(reverse('lab_9:register'), self.credentials, follow=True)
        self.assertRedirects(response1, reverse('lab_9:index'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)

        response2 = self.client.get(reverse('lab_9:register'), follow=True)
        self.assertRedirects(response2, reverse('lab_9:index'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
    def test_unsuccessful_register(self):
        response = self.client.post(reverse('lab_9:register'), self.falseCredentials, follow=True)
        self.assertEquals(response.status_code, 400)
        self.assertTemplateUsed(response, 'lab_9/register.html')

class TestLogout(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret',
        }
        User.objects.create_user(**self.credentials)
    def test_logout(self):
        response1 = self.client.post(reverse('lab_9:login'), self.credentials, follow=True)
        self.assertRedirects(response1, reverse('lab_9:index'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)

        response2 = self.client.get(reverse('lab_9:logout'), follow=True)
        self.assertRedirects(response2, reverse('lab_9:login'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
