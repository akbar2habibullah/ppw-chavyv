from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.urls import reverse

# Create your views here.

def index(req):
    if req.user.is_authenticated:
        return render(req, 'lab_9/index.html', {'page_title': 'Private Page', 'username': req.user.username})
    else:
        return redirect(reverse('lab_9:login'))

def login(req):
    if req.user.is_authenticated:
        return redirect(reverse('lab_9:index'))
    if req.method == 'POST':
        username = req.POST['username']
        password = req.POST['password']
        user = authenticate(req, username=username, password=password)
        if user is not None:
            auth_login(req, user)
            return redirect('lab_9:index')
        else:
            form = AuthenticationForm()
            return render(req, 'lab_9/login.html', {'form': form, 'page_title': 'Login Page', 'error_msg': 'The E-mail and Password do not match'}, status=401)
    else:
        form = AuthenticationForm()
        return render(req, 'lab_9/login.html', {'form': form, 'page_title': 'Login Page', 'error_msg': ''})

def register(req):
    if req.user.is_authenticated:
        return redirect(reverse('lab_9:index'))
    if req.method == 'POST':
        form = UserCreationForm(req.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(req, username=username, password=password)
            auth_login(req, user)
            return redirect(reverse('lab_9:index'))
        else:
            return render(req, 'lab_9/register.html', {'form': form, 'page_title': 'Register Page'}, status=400)
    else:
        form = UserCreationForm()
        return render(req, 'lab_9/register.html', {'form': form, 'page_title': 'Register Page'})

def logout(req):
    auth_logout(req)
    return redirect(reverse('lab_9:login'))
