$(document).ready(function () {
  if ($.cookie("dark-mode") != null) {
    darking();
  }
  $("#dark-mode-btn").click(darking);
});

const darking = () => {
  $("body").toggleClass("dark-mode");
  $("#dark-mode-btn").toggleClass("fa-moon");
  $("#dark-mode-btn").toggleClass("fa-sun");
  if ($("body").hasClass("dark-mode")) {
    $.cookie("dark-mode", "yes", { path: "/", expires: 365 });
  } else {
    $.removeCookie("dark-mode", { path: "/" });
  }
};
