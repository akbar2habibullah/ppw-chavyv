$(document).ready(function () {
  $("#button-addon").click(loadData);
});

const loadData = () => {
  const input = document.getElementById("input-title").value;

  fetch(`https://www.googleapis.com/books/v1/volumes?q=${input}`)
    .then((res) => res.json())
    .then((data) => {
      let books = '';

      data.items.forEach((book) => {
        books += `
          <div class="col-6 p-1">
            <div class="card">
              <img src="${book.volumeInfo.imageLinks ? book.volumeInfo.imageLinks.thumbnail : '/static/assets/unknown.jpg'}" class="card-img-top img-thumbnail rounded mx-auto d-bloc" alt="..." />
              <div class="card-body">
                <h5 class="card-title">${book.volumeInfo.title}</h5>
                <p class="card-text">
                  ${book.volumeInfo.subtitle ? book.volumeInfo.subtitle : ''}
                </p>
              </div>
              <ul class="list-group list-group-flush">
                <li class="list-group-item content">Author(s): ${book.volumeInfo.authors ? book.volumeInfo.authors.join(', ') : 'Unknown'}</li>
                <li class="list-group-item content">Page Count: ${book.volumeInfo.pageCount ? book.volumeInfo.pageCount : 'Unknown'}</li>
                <li class="list-group-item content">Published Date: ${book.volumeInfo.publishedDate ? book.volumeInfo.publishedDate : 'Unknown'}</li>
              </ul>
            </div>
          </div>
        `;
      });

      document.getElementById('data-books').innerHTML = books;

      console.log(data.items);
    });
};