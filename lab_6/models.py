from django.db import models

# Create your models here.

class Kegiatan(models.Model): # pragma: no cover
    nama = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

class Peserta(models.Model): # pragma: no cover
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama = models.CharField(max_length=255)

    def __str__(self):
        return self.nama
