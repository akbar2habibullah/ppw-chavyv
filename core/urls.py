"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('lab_0.urls')),
    path('lab_1/', include('lab_1.urls')),
    path('lab_2/', include('lab_2.urls')),
    path('lab_3/', include('lab_3.urls')),
    path('lab_4/', include('lab_4.urls')),
    path('lab_5/', include('lab_5.urls')),
    path('lab_6/', include('lab_6.urls')),
    path('lab_7/', include('lab_7.urls')),
    path('lab_8/', include('lab_8.urls')),
    path('lab_9/', include('lab_9.urls')),
]
