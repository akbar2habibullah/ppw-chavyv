from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.


def index(req):
    if req.method == 'POST':
        form = forms.MatkulForm(req.POST)
        if form.is_valid():
            form.save()
            return redirect('/lab_5/matkul/')
    else:
        form = forms.MatkulForm()
        return render(req, 'lab_5/index.html', {'page_title': 'Story 5', 'form': form})


def matkul(req):
    all_matkul = models.Matkul.objects.all().values()
    return render(req, 'lab_5/matkul.html', {'page_title': 'Daftar Matkul', 'all_matkul': all_matkul})

def detail(req, pk): # pragma: no cover
    try:
        matkul = models.Matkul.objects.get(pk=pk)
        return render(req, 'lab_5/detail.html', {'page_title': 'Detail Matkul', 'matkul': matkul})
    except models.Matkul.DoesNotExist:
        return redirect('/lab_5/matkul/')

def delete(req, pk): # pragma: no cover
    try:
        matkul = models.Matkul.objects.get(pk=pk)
        matkul.delete()
        return redirect('/lab_5/matkul/')
    except models.Matkul.DoesNotExist:
        return redirect('/lab_5/matkul/')