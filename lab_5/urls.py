from django.urls import path
from . import views

app_name = 'lab_5'

urlpatterns = [
    path('', views.index, name='index'),
    path('matkul/', views.matkul, name='matkul'),
    path('detail/<int:pk>/', views.detail, name='detail'),
    path('delete/<int:pk>/', views.delete, name='delete')
]
