from django.db import models

# Create your models here.

class Matkul(models.Model):
    nama = models.CharField(max_length=255, help_text='Masukan nama matkul')
    dosen = models.CharField(max_length=255, help_text='Masukan nama dosen')
    sks = models.IntegerField(help_text='Masukan jumlah SKS')
    deskripsi = models.TextField(help_text='Masukan deskripsi')
    semester = models.CharField(max_length=255, choices=[(
        'GASAL', 'Gasal 2020/2021'), ('GENAP', 'Genap 2020/2021')], help_text='Pilih semester')
    ruang = models.CharField(max_length=255, help_text='Masukan ruang kelas')
