from django.forms import ModelForm
from . import models


class MatkulForm(ModelForm):
    class Meta:
        model = models.Matkul
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'semester', 'ruang']
