from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views, models


class TestUrls(TestCase):  # pragma: no cover
    def test_index_url_resolved(self):
        url = reverse('lab_5:index')
        self.assertEquals(resolve(url).func, views.index)

    def test_matkul_url_resolved(self):
        url = reverse('lab_5:matkul')
        self.assertEquals(resolve(url).func, views.matkul)

    def test_detail_url_resolved(self):
        new_matkul = models.Matkul.objects.create(
            nama='PPW', dosen='Bu Ara', sks=3, deskripsi='Perancangan dan pengembangan web', semester='GENAP', ruang='2.2301')
        url = reverse('lab_5:detail', args=[1])
        self.assertEquals(resolve(url).func, views.detail)

    def test_matkul_delete_resolved(self):
        new_matkul = models.Matkul.objects.create(
            nama='PPW', dosen='Bu Ara', sks=3, deskripsi='Perancangan dan pengembangan web', semester='GENAP', ruang='2.2301')
        url = reverse('lab_5:delete', args=[1])
        self.assertEquals(resolve(url).func, views.delete)

class TestViews(TestCase):  # pragma: no cover
    def test_index_GET(self):
        client = Client()
        response = client.get(reverse('lab_5:index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'lab_5/index.html')

    def test_index_POST(self):
        client = Client()
        response = client.post(reverse('lab_5:index'), data={
                               'nama': 'PPW', 'dosen': 'Bu Ara', 'sks': 3, 'deskripsi': 'Perancangan dan pengembangan web', 'semester': 'GENAP', 'ruang': '2.2301'})
        counting_all_matkul = models.Matkul.objects.all().count()
        self.assertEquals(counting_all_matkul, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], reverse('lab_5:matkul'))

    def test_matkul_GET(self):
        client = Client()
        response = client.get(reverse('lab_5:matkul'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'lab_5/matkul.html')

class TestModels(TestCase):  # pragma: no cover
    def test_matkul_model(self):
        new_matkul = models.Matkul.objects.create(
            nama='PPW', dosen='Bu Ara', sks=3, deskripsi='Perancangan dan pengembangan web', semester='GENAP', ruang='2.2301')
        counting_all_matkul = models.Matkul.objects.all().count()
        self.assertEquals(counting_all_matkul, 1)
