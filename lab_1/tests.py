from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views

class TestUrls(TestCase): # pragma: no cover
    def test_index_url_resolved(self):
        url = reverse('lab_1:index')
        self.assertEquals(resolve(url).func, views.index)

class TestViews(TestCase): # pragma: no cover
    def test_index_GET(self):
        client = Client()
        response = client.get(reverse('lab_1:index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'lab_1/index.html')