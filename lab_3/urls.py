from django.urls import path
from . import views

app_name = 'lab_3'

urlpatterns = [
    path('', views.index, name='index'),
]