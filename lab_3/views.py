from django.shortcuts import render

# Create your views here.

def index(req):
    return render(req, 'lab_3/index.html', {'page_title': 'Story 3'})