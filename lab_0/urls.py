from django.urls import path
from . import views

app_name = 'lab_0'

urlpatterns = [
    path('', views.index, name='index'),
    path('ppw/', views.ppw, name='ppw')
]