from django.shortcuts import render

# Create your views here.

def index(req):
    return render(req, 'lab_0/index.html', {'page_title': 'Home'})

def ppw(req):
    return render(req, 'lab_0/ppw.html', {'page_title': 'Tugas-tugas PPW'})