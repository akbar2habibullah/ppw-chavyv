from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views

class TestUrls(TestCase): # pragma: no cover
    def test_home_url_resolved(self):
        url = reverse('lab_0:index')
        self.assertEquals(resolve(url).func, views.index)

    def test_ppw_url_resolved(self):
        url = reverse('lab_0:ppw')
        self.assertEquals(resolve(url).func, views.ppw)

class TestViews(TestCase): # pragma: no cover
    def test_index_GET(self):
        client = Client()
        response = client.get(reverse('lab_0:index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'lab_0/index.html')

    def test_ppw_GET(self):
        client = Client()
        response = client.get(reverse('lab_0:ppw'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'lab_0/ppw.html')